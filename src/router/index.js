import { createRouter, createWebHistory } from 'vue-router'
import AboutView from '@/views/AboutView.vue'
import PortfolioView from '@/views/PortfolioView.vue'
import ResumeView from '@/views/ResumeView.vue'
import AdminView from '@/views/AdminView.vue'

const routes = [

    {
        name: "about",
        path: '/',
        component: AboutView,
    },
    {
        name: "portfolio",
        path: '/portfolio',
        component: PortfolioView,
    },
    {
        name: "resume",
        path: '/resume',
        component: ResumeView,
    },
    {
        name: "admin",
        path: '/admin',
        component: AdminView,
    },

]

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
    linkActiveClass: 'active-link',
    linkExactActiveClass: 'exact-active-link',
})

export default router