# mon site perso - partie frontend

## badges
![status](https://gitlab.com/my-website201405/my-website-frontend/badges/main/pipeline.svg?ignore_skipped=true)
[![coverage](https://gitlab.com/my-website201405/my-website-frontend/badges/8-coverage-report/coverage.svg?job=coverage)](https://my-website201405.gitlab.io/my-website-frontend/)

- état du déploiement (à faire plus tard)


## contenu attendu
- présentation succinte
- portfolio
- CV
- CTA envoi de mail

## technologies utilisées
- vue
- vite

## lancement du projet pour développer
pour l'instant pas de consigne


## déploiement du projet
pour l'instant pas de consigne

## roadmap:
- [x] config de base
- [ ] établir un système de design
    + [ ] composants courants (boutons, champs, )
    + [ ] couleurs
- [ ] vue de présentation
- [ ] vue et composants du CV
- [ ] vue et composants du portfolio
- [ ] composant d'envoi de mail de contact