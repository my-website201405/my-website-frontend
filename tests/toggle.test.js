import { describe, it, assert, expect, beforeEach } from 'vitest'
import { mount } from '@vue/test-utils'
import Toggle from '@/components/Toggle.vue'

describe('toggle tests', () => {

    let wrapper

    beforeEach(() => {
        localStorage.setItem("optionStore", true)
        wrapper = mount(Toggle, {
            props: {
                storeName: "optionStore"
            }
        })
    })
    afterEach(() => {
        localStorage.clear()
    })

    it('should render', () => {
        expect(wrapper.find('.single-toggle-container').exists()).toBe(true)
        expect(wrapper.find('.outer-switch').exists()).toBe(true)
        expect(wrapper.find('.inner-switch').exists()).toBe(true)
    })

    it('should have status from localStorage', () => {
        expect(wrapper.vm.value).toBe(localStorage.getItem("optionStore"))
    })

    it('should render its variable as attribute in DocumentElement and it should be the same as in localStorage', () => {
        expect(document.documentElement.getAttribute("optionStore") === 'true').toBe(localStorage.getItem("optionStore"))
    })

    it('should change state in data when clicked', async () => {

        const initialValue = wrapper.vm.value
        const clickable = wrapper.find('.outer-switch')

        await clickable.trigger('click')

        const finalValue = wrapper.vm.value
        expect(finalValue).toBe(!initialValue)
    })

    // it('should change state in document.documentElement when clicked', async () => {

    //     console.log(document.documentElement.outerHTML)

    //     const initialValue = document.documentElement.getAttribute("optionStore")

    //     const clickable = wrapper.find('.outer-switch')
    //     await clickable.trigger('click')

    //     console.log(document.documentElement.outerHTML)
    //     const finalValue = document.documentElement.getAttribute("optionStore")

    //     const boolValue = initialValue === "true" ? "false" : (initialValue === "false" ? "True" : "error")


    //     expect(finalValue).toBe(boolValue)
    // })

    // it('should change state in LocalStorage when clicked', async () => {

    //     const initialValue = localStorage.getItem("optionStore")

    //     console.log(`initialValue ${initialValue}`)
    //     const clickable = wrapper.find('.outer-switch')
    //     await clickable.trigger('click')

    //     const finalValue = localStorage.getItem("optionStore")
    //     console.log(`finalValue ${finalValue}`)
    //     expect(finalValue).toBe(!initialValue)
    // })






})